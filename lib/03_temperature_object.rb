class Temperature

  def self.from_fahrenheit(degree)
    Temperature.new(f: degree)
  end

  def self.from_celsius(degree)
    Temperature.new(c: degree)
  end

  def initialize(c: nil, f: nil)
    @c = c
    @f = f
  end

  def in_fahrenheit
    raise "no degree info" if @c.nil? && @f.nil?
    return @f unless @f.nil?
    @c * 9.0 / 5 + 32
  end

  def in_celsius
    raise "no degree info" if @c.nil? && @f.nil?
    return @c unless @c.nil?
    (@f - 32) * 5 / 9.0
  end

end

class Fahrenheit < Temperature
  def initialize(f)
    @f = f
  end
end

class Celsius < Temperature
  def initialize(c)
    @c = c
  end
end
