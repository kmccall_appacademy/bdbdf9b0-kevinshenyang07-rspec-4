class Timer

  SECONDS_OF_DAY = 24 * 60 * 60

  attr_accessor :seconds

  def initialize(seconds=0)
    @seconds = seconds
  end

  def padded(num)
    raise "input out of bound" if num > 60 or num < 0
    if num < 10
      "0" + num.to_s
    else
      num.to_s
    end
  end

  def time_string
    @seconds = @seconds % SECONDS_OF_DAY
    hours = @seconds / 3600
    minutes = @seconds % 3600 / 60
    secs = @seconds % 3600 % 60
    [self.padded(hours), self.padded(minutes), self.padded(secs)].join(":")
  end

end
