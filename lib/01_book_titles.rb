class Book

  def initialize(title=nil)
    @title = title
  end

  def titlize(title)
    words = title.split(" ")
    small_words = ["a", "the", "an", "and", "of", "in"]
    words = words.map { |w| small_words.include?(w) ? w : w.capitalize }
    words[0] = words[0].capitalize
    words.join(" ")
  end

  def title
    @title
  end

  def title=(value)
    @title = titlize(value)
  end

end
